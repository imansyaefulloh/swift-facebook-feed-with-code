//
//  ViewController.swift
//  ISFacebookFeedWithCode
//
//  Created by Iman Syaefulloh on 7/10/16.
//  Copyright © 2016 imansyaefulloh. All rights reserved.
//

import UIKit

let cellId = "cellId"

class Post: NSObject {
    
    var name: String?
    var profileImageName: String?
    var statusText: String?
    var statusImageName: String?
    
    var numLikes: NSNumber?
    var numComments: NSNumber?
    
    var statusImageUrl: String?
    
    var location: Location?
}

class Location: NSObject {
    var city: String?
    var state: String?
}

class FeedController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var posts = [Post]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // cache every URL request till 500MB
//        let memoryCapacity = 500 * 1024 * 1024
//        let diskCapacity = 500 * 1024 * 1024
//        let urlCache = NSURLCache(memoryCapacity: memoryCapacity, diskCapacity: diskCapacity, diskPath: "myDiskPath")
//        NSURLCache.setSharedURLCache(urlCache)
//        
//        
//        if let path = NSBundle.mainBundle().pathForResource("single_post", ofType: "json") {
//            
//            do {
//                
//                let data = try(NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingMappedIfSafe))
//                
//                let jsonDictionary = try(NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers))
//                
//                if let postDictionary = jsonDictionary["post"] as? [String: AnyObject] {
//                    let post = Post()
//                    post.setValuesForKeysWithDictionary(postDictionary)
//                    print(post.name, post.statusText)
//                }
//                
//                print(jsonDictionary)
//                
//                
//            } catch let error {
//                print(error)
//            }
//            
//        }
        
        let postMark = Post()
        postMark.name = "Mark Zuckenberg"
        postMark.statusText = "Meanwhile, Best turned into the dark side."
        postMark.profileImageName = "zuckprofile"
        postMark.statusImageName = "zuckdog"
        postMark.numLikes = 400
        postMark.numComments = 123
        postMark.statusImageUrl = "https://s3-us-west-2.amazonaws.com/letsbuildthatapp/mark_zuckerberg_background.jpg"
        
        let postSteve = Post()
        postSteve.name = "Steve Jobs"
        postSteve.statusText = "Design is not just what it looks like and feels like. Design is how it works.\n\n" + "Being the richest man in the cemetery does not matter to me. Going to bed at night saying we have something wonderfull, that is what matter to me.\n\n" + "Sometime when you inovate, you make mistakes. It is best to admin them quickly, and get on with improving your other innovations."
        postSteve.profileImageName = "steve_profile"
        postSteve.statusImageName = "steve_status"
        postSteve.numLikes = 1000
        postSteve.numComments = 55
        postSteve.statusImageUrl = "https://s3-us-west-2.amazonaws.com/letsbuildthatapp/steve_jobs_background.jpg"
        
        let postGandhi = Post()
        postGandhi.name = "Mahatma Gandhi"
        postGandhi.statusText = "Live as if you were to die tomorrow; Learn as if you were live forever;\n" + "The weak can never forgive. Forgiveness is the attributes of the strong.\n" + "Happiness is when what you think, what you say, and what you do are in harmony."
        postGandhi.profileImageName = "gandhi"
        postGandhi.statusImageName = "gandhi_status"
        postGandhi.numLikes = 333
        postGandhi.numComments = 22
        postGandhi.statusImageUrl = "https://s3-us-west-2.amazonaws.com/letsbuildthatapp/gandhi_status.jpg"
        
        posts.append(postMark)
        posts.append(postSteve)
        posts.append(postGandhi)
        
        navigationItem.title = "Facebook Feeds"
        
        collectionView?.alwaysBounceVertical = true
        
        collectionView?.backgroundColor = UIColor(white: 0.95, alpha: 1)
        
        collectionView?.registerClass(FeedCell.self, forCellWithReuseIdentifier: cellId)
    }

    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let feedCell =  collectionView.dequeueReusableCellWithReuseIdentifier(cellId, forIndexPath: indexPath) as! FeedCell
        
        feedCell.post = posts[indexPath.item]
        
        feedCell.feedController = self
        
        return feedCell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        if let statusText = posts[indexPath.item].statusText {
            
            let rect = NSString(string: statusText).boundingRectWithSize(
                CGSizeMake(view.frame.width, 1000),
                options: NSStringDrawingOptions.UsesFontLeading.union(NSStringDrawingOptions.UsesLineFragmentOrigin),
                attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14)],
                context: nil
            )
            
            let knownHeight: CGFloat = 8 + 44 + 4 + 4 + 200 + 8 + 24 + 8 + 44
            
            return CGSizeMake(view.frame.width, rect.height + knownHeight + 24)
        }
        
        return CGSizeMake(view.frame.width, 500)
        
    }


    // fix layout when orientation changed
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    let zoomImageView = UIImageView()
    
    let blackBackgroundView = UIView()

    let navBarCoverView = UIView()
    
    let tabBarCoverView = UIView()
    
    var statusImageView = UIImageView()

    
    func animateImageView(statusImageView: UIImageView) {
        
        self.statusImageView = statusImageView
        
        if let startingFrame = statusImageView.superview?.convertRect(statusImageView.frame, toView: nil) {
            
            // hide original image
            statusImageView.alpha = 0
            
            blackBackgroundView.frame = self.view.frame
            blackBackgroundView.backgroundColor = UIColor.blackColor()
            blackBackgroundView.alpha = 0
            view.addSubview(blackBackgroundView)
            
            blackBackgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FeedController.zoomOut)))
            
            navBarCoverView.frame = CGRectMake(0, 0, 1000, 20 + 44)
            navBarCoverView.backgroundColor = UIColor.blackColor()
            navBarCoverView.alpha = 0
            
            navBarCoverView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FeedController.zoomOut)))
            
            if let keyWindow = UIApplication.sharedApplication().keyWindow {
                keyWindow.addSubview(navBarCoverView)
                
                tabBarCoverView.frame = CGRectMake(0, keyWindow.frame.height - 49, 1000, 49)
                tabBarCoverView.backgroundColor = UIColor.blackColor()
                tabBarCoverView.alpha = 0
                
                keyWindow.addSubview(tabBarCoverView)
                
                tabBarCoverView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FeedController.zoomOut)))
                
            }
            
            zoomImageView.backgroundColor = UIColor.redColor()
            zoomImageView.frame = startingFrame
            zoomImageView.userInteractionEnabled = true
            zoomImageView.image = statusImageView.image
            zoomImageView.contentMode = .ScaleAspectFill
            zoomImageView.clipsToBounds = true
                
            view.addSubview(zoomImageView)
            
            zoomImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FeedController.zoomOut)))
            
            UIView.animateWithDuration(0.75, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: .CurveEaseOut, animations: {
                
                    let height = (self.view.frame.width / startingFrame.width) * startingFrame.height
                    
                    let y = (self.view.frame.height / 2) - (height / 2)
                    
                    self.zoomImageView.frame = CGRectMake(0, y, self.view.frame.width, height)
                    
                    self.blackBackgroundView.alpha = 1
                    
                    self.navBarCoverView.alpha = 1
                    
                    self.tabBarCoverView.alpha = 1
                
                }, completion: nil)
            
            
        }
        
    }
    
    func zoomOut() {
        
        if let startingFrame = statusImageView.superview?.convertRect(statusImageView.frame, toView: nil) {
            
            UIView.animateWithDuration(0.75, animations: { 
                
                    self.zoomImageView.frame = startingFrame
                
                    self.blackBackgroundView.alpha = 0
                    self.navBarCoverView.alpha = 0
                    self.tabBarCoverView.alpha = 0
                
                }, completion: { (didComplete) in
                    
                    self.zoomImageView.removeFromSuperview()
                    self.blackBackgroundView.removeFromSuperview()
                    self.navBarCoverView.removeFromSuperview()
                    self.tabBarCoverView.removeFromSuperview()
                    
                    self.statusImageView.alpha = 1
            })
            
        }
        
    }
}


class FeedCell: UICollectionViewCell {
    
    var feedController: FeedController?
    
    var post: Post? {
        
        didSet {
            
            statusImageView.image = nil
            loader.startAnimating()
            
            if let name = post?.name {
                let attributedText = NSMutableAttributedString(string: name, attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(14)])
                
                attributedText.appendAttributedString(NSAttributedString(string: "\nDecember 18 - San Francisco - ", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(12), NSForegroundColorAttributeName:
                    UIColor.rgb(155, green: 161, blue: 171)]))
                
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineSpacing = 5
                
                attributedText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, attributedText.string.characters.count))
                
                let attachment = NSTextAttachment()
                attachment.image = UIImage(named: "globe_small")
                attachment.bounds = CGRectMake(0, -2, 12, 12)
                attributedText.appendAttributedString(NSAttributedString(attachment: attachment))
                
                nameLabel.attributedText = attributedText
            }
            
            if let statusText = post?.statusText {
                statusTextView.text = statusText
            }
            
            if let profileImangeName = post?.profileImageName {
                profileImageView.image = UIImage(named: profileImangeName)
            }
            
            if let statusImageUrl = post?.statusImageUrl {
                
                NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: statusImageUrl)!, completionHandler: { (data, response, error) -> Void in
                    
                    if error != nil {
                        print(error)
                        return
                    }
                    
                    let image = UIImage(data: data!)
                    
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.statusImageView.image = image
                        self.loader.stopAnimating()
                    }
                    
                    
                }).resume()
                
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let nameLabel: UILabel = {
        
        let label = UILabel()
        label.numberOfLines = 2
        
        return label
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "zuckprofile")
        imageView.contentMode = .ScaleAspectFit
        return imageView
    }()
    
    let statusTextView: UITextView = {
        let textView = UITextView()
        textView.text = "Meanwhile, Beast turnet into the dark side"
        textView.font = UIFont.systemFontOfSize(14)
        textView.scrollEnabled = false
        return textView
    }()
    
    let statusImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "zuckdog")
        imageView.contentMode = .ScaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.userInteractionEnabled = true
        
        return imageView
    }()
    
    let likesCommentsLabel: UILabel = {
        let label = UILabel()
        label.text = "488 Likes     10.7K Comments"
        label.font = UIFont.systemFontOfSize(12)
        label.textColor = UIColor.rgb(155, green: 161, blue: 171)
        return label
    }()
    
    let dividerLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(226, green: 228, blue: 232)
        return view
    }()
    
    
    
    let likeButton: UIButton = FeedCell.buttonForTitle("Like", imageName: "like")
    let commentButton: UIButton = FeedCell.buttonForTitle("Comment", imageName: "comment")
    let shareButton: UIButton = FeedCell.buttonForTitle("Share", imageName: "share")
    
    static func buttonForTitle(title: String, imageName: String) -> UIButton {
        
        let button = UIButton()
        button.setTitle(title, forState: .Normal)
        button.setTitleColor(UIColor.rgb(143, green: 150, blue: 163), forState: .Normal)
        
        button.setImage(UIImage(named: imageName), forState: .Normal)
        
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0)
        
        button.titleLabel?.font = UIFont.boldSystemFontOfSize(14)
        
        return button
    }
    
    func setupViews() {
        backgroundColor = UIColor.whiteColor()
        
        addSubview(nameLabel)
        addSubview(profileImageView)
        addSubview(statusTextView)
        addSubview(statusImageView)
        addSubview(likesCommentsLabel)
        addSubview(dividerLineView)
        
        addSubview(likeButton)
        addSubview(commentButton)
        addSubview(shareButton)
        
        setupStatusImageViewLoader()
        
        statusImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FeedCell.animate)))
        
        addConstraintWithFormat("H:|-8-[v0(44)]-8-[v1]|", views: profileImageView, nameLabel)
        
        addConstraintWithFormat("H:|-4-[v0]-4-|", views: statusTextView)
        
        addConstraintWithFormat("H:|[v0]|", views: statusImageView)
        
        addConstraintWithFormat("H:|-12-[v0]|", views: likesCommentsLabel)
        
        addConstraintWithFormat("H:|-12-[v0]-12-|", views: dividerLineView)
        
        // button constraint
        addConstraintWithFormat("H:|[v0(v2)][v1(v2)][v2]|", views: likeButton, commentButton, shareButton)
        
        
        addConstraintWithFormat("V:|-12-[v0]", views: nameLabel)
        addConstraintWithFormat("V:|-8-[v0(44)]-4-[v1]-4-[v2(200)]-8-[v3(24)]-8-[v4(0.4)][v5(44)]|", views: profileImageView, statusTextView, statusImageView, likesCommentsLabel, dividerLineView, likeButton)
        
        addConstraintWithFormat("V:[v0(44)]|", views: commentButton)
        addConstraintWithFormat("V:[v0(44)]|", views: shareButton)
        
    }
    
    let loader = UIActivityIndicatorView(activityIndicatorStyle: .White)
    
    func setupStatusImageViewLoader() {
        
        loader.hidesWhenStopped = true
        loader.startAnimating()
        loader.color = UIColor.blackColor()
        statusImageView.addSubview(loader)
        
        statusImageView.addConstraintWithFormat("H:|[v0]|", views: loader)
        statusImageView.addConstraintWithFormat("V:|[v0]|", views: loader)
    }
    
    func animate() {
        feedController?.animateImageView(statusImageView)
    }
    
}

extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}

extension UIView {
    
    func addConstraintWithFormat(format: String, views: UIView...) {
        var viewDictionary = [String: UIView]()
        for (index, view) in views.enumerate() {
            let key = "v\(index)"
            viewDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(format, options: NSLayoutFormatOptions(), metrics: nil, views: viewDictionary))
    }
    
}






















