//
//  CustomTabBarController.swift
//  ISFacebookFeedWithCode
//
//  Created by Iman Syaefulloh on 7/10/16.
//  Copyright © 2016 imansyaefulloh. All rights reserved.
//

import Foundation
import UIKit

class CustomTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let feedController = FeedController(collectionViewLayout: UICollectionViewFlowLayout())
        let navigationController = UINavigationController(rootViewController: feedController)
        
        navigationController.title = "News Feed"
        navigationController.tabBarItem.image = UIImage(named: "news_feed_icon")
        
        let friendRequestController = FriendRequestsController()
        let secondNavigationController = UINavigationController(rootViewController: friendRequestController)
        secondNavigationController.title = "Request"
        secondNavigationController.tabBarItem.image = UIImage(named: "requests_icon")
        
        let messengerController = UIViewController()
        messengerController.navigationItem.title = "Messenger"
        messengerController.view.backgroundColor = UIColor.whiteColor()
        let messengerNavigationController = UINavigationController(rootViewController: messengerController)
        messengerNavigationController.title = "Messenger"
        messengerNavigationController.tabBarItem.image = UIImage(named: "messenger_icon")
        
        let notificationController = UIViewController()
        notificationController.navigationItem.title = "Notifications"
        notificationController.view.backgroundColor = UIColor.whiteColor()
        let notificationNavigationController = UINavigationController(rootViewController: notificationController)
        notificationNavigationController.title = "Notifications"
        notificationNavigationController.tabBarItem.image = UIImage(named: "globe_icon")
        
        let moreController = UIViewController()
        moreController.navigationItem.title = "More"
        moreController.view.backgroundColor = UIColor.whiteColor()
        let moreNavigationController = UINavigationController(rootViewController: moreController)
        moreNavigationController.title = "More"
        moreNavigationController.tabBarItem.image = UIImage(named: "more_icon")
        
        viewControllers = [navigationController, secondNavigationController, messengerNavigationController, notificationNavigationController, moreNavigationController]
        
        tabBar.translucent = false
        
        let topBorder = CALayer()
        topBorder.frame = CGRectMake(0, 0, 1000, 0.5)
        topBorder.backgroundColor = UIColor.rgb(229, green: 231, blue: 235).CGColor
        
        tabBar.layer.addSublayer(topBorder)
        tabBar.clipsToBounds = true
        
    }
    
}












